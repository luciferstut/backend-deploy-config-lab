## Overview
This repo is designed to give description of each environment of each country. 

## Some useful usage
Please install *kubectl* and *kustomize* first. 

### Deploy several modules. CD to your environment
`kustomize build . --load_restrictor none | kubectl apply -f -`
### AFTER kustomize 4.0.1, Deploy several modules. CD to your environment
`kustomize build . --load-restrictor LoadRestrictionsNone | kubectl apply -f -`

### Create secret from env (please do not commit original secret to git!!!)
`kubectl create secret generic shared-secrets --dry-run=client --from-env-file=shared-secret.env -o yaml | kubeseal --format yaml --namespace my-aaclub > shared-secrets.yml`
`kubectl create secret generic test-cluster-secrets --dry-run=client --from-env-file=content-operation-secrets.env -o yaml | kubeseal --format yaml --scope cluster-wide > test-cluser-secrets.yml`

### Add secret entry 
`echo -n my-password | kubeseal --raw --from-file=/dev/stdin --namespace my-aaclub --name password`


### A way to export env from a secret
`kubectl get secret -o go-template='{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}' -n aaclub-my shared-configs`



### topology of modules
0 url-shortener user-tag myinfo merchant-center-account oss message 
5 qrcode ocr sftp-client user
10 merchant ab-test referral push sms security-verification
20 credit-application product voucher loyalty merchandise user-digital-goods lucky-draw
30 finance-product settlement configurable-page shipping-service 
40 payment campaign growth-express gift
50 open-api shopify-business wix-integration voucher-shop
60 order shopify-router product-crawler lazada-cooperate
70 notification data content-operation data-clean
80 app mboss oboss metchant-center miscellaneous-service



### contents to edit when submitting a MR
0. /overlays/staging|production/*/configs/*-configs|secrets.yml to add/delete/update your configs
    - for *-secrets.yml, ask RM to encrypt the sealedsecrets for you

1. (optional) if you added new application yml, for example scheduled.yml
    a) edit bases/{your_module_name}/kustomization.yml to include your new yml in 'resources'
    b) edit /overlays/staging|production/kustomization.yml to customize your image version, 'staging' for staging, 'master' for production
    
2. /overlays/staging|production/*/configs/kustomization.yml to include your edited configs yml

3. /overlays/staging|production/*/kustomization.yml to include your modules in 'resources'

4. edit checklist confluence of related version

5. (RM) only: 
   /overlays/staging|production/kustomization.yml update release version of /spec/template/metadata/labels/aaclub-version 
