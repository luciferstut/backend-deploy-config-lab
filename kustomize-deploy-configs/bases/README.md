# Overview 
This folder contains all bases yaml about our service. We manage bases by each module basis. Please note that the definition of module is on the deployment point of view, and might be different to our gitlab repo. 

A yml seems long. However it is highly templated. It only contains two useful informations: 
- service name
- docker image

# A handy way to generate a resource file. 
`cat api-template.yml | sed 's/{{SERVICE_NAME}}/new-text/g' | sed 's+{{DOCKER_IMAGE}}+harbor.aaflamingo.net/aaclub/order-backend/application-internal-api:staging+g' > folder/internal-api.yml`

